var env = function () {

    this.port = 8080;
    this.baseUrl = "http://localhost:" + this.port;
    this.gameObjects = "gameObjects/3DModels/";

    return this;
};

if(module.exports)
    exports = module.exports = env;
