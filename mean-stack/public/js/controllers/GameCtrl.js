angular.module('GameCtrl', []).controller('GameController', function (authUser, $scope, $location, socket, ENEMIES) {

    console.log("The game started.");

    AFRAME.registerComponent('log', {
        schema: {type: 'string'},
        init: function () {
            var stringToLog = this.data;
            console.log(stringToLog);
        }
    });

    // var extendDeep = AFRAME.utils.extendDeep;
    // The mesh mixin provides common material properties for creating mesh-based primitives.
    // This makes the material component a default component and maps all the base material properties.
    // var meshMixin = AFRAME.primitives.getMeshMixin();

    // Component to change to random color on click.
    AFRAME.registerComponent('cursor-listener', {
        init: function () {

            var COLORS = ['red', 'green', 'blue'];

            // Substract the weapon hit points from the enemy life points
            this.el.addEventListener('click', function (evt) {

                var target = this; // the target                
                var enemy = $(target).attr("enemy");
                var character = $("a-character").attr("character");

                console.log("enemy", enemy);
                console.log("character", character);
                enemy.healthpoints -= character.hitpoints;

                var randomIndex = Math.floor(Math.random() * COLORS.length);
                target.setAttribute('material', 'color', COLORS[randomIndex]);
                console.log("%c Enemy hit!", 'color: orange; text-transform: uppercase;');
            });
        },
        update: function () {
            // Do something when component's data is updated.

        },
        remove: function () {
            // Do something the component or its entity is detached.
        },
        tick: function (time, timeDelta) {
            // Do something on every scene tick or frame.
        }
    });

    // Register character primitive
    AFRAME.registerPrimitive('a-character', {
        // Attaches the `character` component by default.
        // Defaults the character to be parallel to the ground.
        defaultComponents: {
            character: {},
            rotation: {
                x: 0,
                y: 0,
                z: 0
            },
            camera: {},
            weapon: {},
            armor: {}
        },
        // Maps HTML attributes to the `character` component's properties.
        mappings: {
            width: 'character.width',
            depth: 'character.depth',
            density: 'character.density',
            color: 'character.color',
            opacity: 'character.opacity',
        }
    });

    // Register component enemy
    AFRAME.registerComponent('character', {
        schema: {
            name: {type: 'string'},
            type: {type: 'string'},
            hitpoints: {type: 'number'},
            healthpoints: {type: 'number'}
        },
        init: function () {
            console.log("character component initialized");
        },
        update: function () {
            // Do something when component's data is updated.
        },
        remove: function () {
            // Do something the component or its entity is detached.
            console.log("The character is dead.");
        },
        tick: function (time, timeDelta) {
            // Do something on every scene tick or frame.
            if (this.data.healthpoints <= 0)
                this.remove();
        }
    });

    // Register component enemy
    AFRAME.registerComponent('enemy', {
        schema: {
            name: {type: 'string'},
            type: {type: 'string'},
            hitpoints: {type: 'number'},
            healthpoints: {type: 'number'}
        },
        init: function () {
            console.log("enemy component initialized");
        },
        update: function () {
            // Do something when component's data is updated.

        },
        remove: function () {
            // Do something the component or its entity is detached.
            $(this.el).remove();
            console.log("The enemy is dead.");
        },
        tick: function (time, timeDelta) {
            // Do something on every scene tick or frame.
            if (this.data.healthpoints <= 0) {
                this.remove();
                return false;
            }
        }
    });

    // Register enemy primitive
    // Register character primitive
    AFRAME.registerPrimitive('a-enemy', {
        // Attaches the `enemy` component by default.
        // Defaults the enemy to be parallel to the ground.
        defaultComponents: {
            enemy: {},
            rotation: {
                x: 0,
                y: 0,
                z: 0
            },
            camera: {},
        },
        // Maps HTML attributes to the `character` component's properties.
        mappings: {
            width: 'enemy.width',
            depth: 'enemy.depth',
            density: 'enemy.density',
            color: 'enemy.color',
            opacity: 'enemy.opacity',
        }
    });

    // Register weapon component
    AFRAME.registerComponent('weapon', {
        schema: {
            name: {type: 'string'},
            type: {type: 'string'},
            hitpoints: {type: 'number'},
            durability: {type: 'number'}
        },
        init: function () {

            if (!this.data.hitPoints) {
                this.data.hitPoints = 20;
            }

            if (!this.data.durability) {
                this.data.durability = 100;
            }

            // Do something when component first attached.
            console.log("%c I got a weapon.", 'color: blue');
        },
        update: function () {
            // Do something when component's data is updated.
            // console.log("My weapon is killing idiots")
        },
        remove: function () {
            // Do something the component or its entity is detached.
            console.log("%c The weapon is broken!", 'color: red');
        },
        tick: function (time, timeDelta) {
            // Do something on every scene tick or frame.
            if (this.data.durability <= 0) {
                this.remove();
                return;
            }
        },
    });

    // Register armor component
    AFRAME.registerComponent('armor', {
        schema: {
            name: {type: 'string'},
            protection: {type: 'number'},
        },
        init: function () {
            // Do something when component first attached.
            if (!this.data.protection)
                this.data.protection = 100;
        },
        update: function () {
            // Do something when component's data is updated.
            console.log("%c Armor = " + this.protection, 'color: green');
        },
        remove: function () {
            // Do something the component or its entity is detached.
            console.log("%c The armor is down", 'color: red');
        },
        tick: function (time, timeDelta) {
            // Do something on every scene tick or frame.
            if (this.data.protection <= 0) {
                this.remove();
            }
        }
    });

    // Eventing
    var entity = $("#character");

    entity.on('stateadded', function (evt) {
        // Check different states [selected, rotate,...]
        if (evt.state === 'selected') {
            console.log('Entity now selected!');
        }
    });

    entity.on('walk', function (evt) {
        console.log("walking...");
        console.log("play walking sound.");
    });

    entity.on('run', function (evt) {
        console.log("running...");
        console.log("play running sound.");
    });

    entity.on('shoot', function (evt) {
        console.log("shooting...");
        console.log("play shooting sound.");
    });

    entity.on('hit', function (evt) {
        console.log("hitting...");
        console.log("play hitting sound.");
    });


    // var enemyToKill;
    // entity[0].emit('kill', {target: enemyToKill}); // Kill the enemy event
    entity[0].addState('selected'); // adding state
    entity[0].is('selected');  // >> true
    entity[0].emit('rotate'); // Triggering the event
});
