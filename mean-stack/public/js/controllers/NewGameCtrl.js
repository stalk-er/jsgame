angular.module('NewGameCtrl', []).controller('NewGameController', function(authUser, $scope, socket, $location) {

	$scope.createGame = function () {

		if(!authUser) {
			console.error("You don't have a valid user. Check the local storage.");
			return false;
		}
		
		var data = {
			gameData:{
				name:$scope.gameName,
				password:$scope.gamePassword,
			},
			player: authUser
		};

		// Request to create a new game with this data
		socket.emit("new game", data);

		// This will fire if the game has been created successfully, which means the user can login
		socket.on('new game created', function (game) {
			$location.url("/game/" + game.id + "/lobby");
		});
	};
});
