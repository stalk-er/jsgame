angular.module('LoginCtrl', []).controller('LoginController', function($scope, socket, $location) {

	$scope.error = false;

	$scope.login = function () {

		if($scope.password != "" && $scope.username != "") {
			var data = {
				id:	Math.random().toString(36).slice(2), // Some random string to use as identifier during the game
				name: $scope.username,
				password: $scope.password
			};
			localStorage.setItem('user', JSON.stringify(data));
			$location.url('/');
		}
		else
			$scope.error = true;

		// Post the data to the server
		// and return response
	};
});
