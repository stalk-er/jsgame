angular.module('GameLobbyCtrl', []).controller('GameLobbyController', function (authUser, $scope, $location,
        socket, $routeParams, $rootScope, gameCharacters, $timeout) {

    var gameId = $routeParams.gameId;

    $scope.players = [];
    $scope.characters = gameCharacters;
    $scope.preview = null;

    $rootScope.$on('$locationChangeStart', function (event, newUrl, oldUrl) {

        console.log(newUrl);
        console.log(oldUrl);

        // Preventing the user from moving throughout the history
        // leaving the game is always possible with the exit button

        //event.preventDefault(); // This prevents the navigation from happening
    });

    // If the game still exists, nobody destroyed the game
    socket.emit("find game by id", gameId);
    socket.on("game exists", function (game) {

        if (game) {
            $scope.gameId = gameId;

            // Pushing all the game players
            for (var p in game.players) {
                $scope.players.push(game.players[p]);
            }
        } else {
            $location.url("/find-game");
        }
    });

    // Show the newly joined users, so everybody sees them
    socket.on("game players", function (players) {
        console.log('PLAYERS', players)

        $scope.players = players;
    });

    // Upon leaving the game
    $scope.leaveGame = function () {

        socket.emit("leave game", {
            gameId: $scope.gameId,
            player: authUser
        });

        socket.on("exit game", function () {
            $location.url("/find-game");
            console.log("Leaving the game...");
        });
    };

    $scope.startGame = function () {

        console.warn("Loading...");
        $timeout(function () {
            $location.url("game/" + gameId);
        }, 1000);
    };

    $scope.characterPreview = function (e, d) {

        var chosenCharacter = $scope.character;
        var chosenCharacterObject = null;
        for (var c in gameCharacters) {
            if (gameCharacters[c].name == chosenCharacter) {
                chosenCharacterObject = gameCharacters[c];
                break;
            }
        }

        if (chosenCharacterObject) {
            $("#characterPreview").html(sceneMarkup(chosenCharacterObject));
        }
        
        // ==================
        var entity = $('#trump')[0];

       

    };

    function sceneMarkup(data) {
        var str = '<a-scene embedded avatar-replayer="spectatorMode: true">' +
                '<a-assets>' +
                '<a-asset-item id="characterPreviewObj" src="' + data.obj + '"></a-asset-item>' +
                '<a-asset-item id="characterPreviewMtl" src="' + data.mtl + '"></a-asset-item>' +
                '</a-assets>' +
                '<a-entity id="trump" look-controls depth="0" scale=".001 .001 .001" obj-model="obj: #characterPreviewObj; mtl: #characterPreviewMtl">' +
                '<a-camera position="' + data.position + '"></a-camera>' +
                '</a-entity>' +
                '</a-scene>';
        return str;
    }
});
