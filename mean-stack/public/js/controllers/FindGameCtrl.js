angular.module('FindGameCtrl', []).controller('FindGameController', function(authUser, $scope, $location, socket) {

	socket.emit('find games', {});
	socket.on('available games', function(games) {
		console.log("Available rooms", games);
		$scope.games = games;
		$scope.$apply();
	});

	$scope.gameAuthenticate = function () {
		socket.emit('authenticate game', $scope.gamePassword);
	};

	socket.on('authenticate', function() {
		console.log("Authenticate, please.");
		jQuery('#gameAuth').modal('show');
		return false;
	});

	// User can join the game
	socket.on('join game', function(gameData) {

		jQuery('#gameAuth').modal('hide');

		// user is allowed to join the game now, redirect him to the game page
		$location.url('/game/' + gameData.id);
	});

	$scope.join = function (gameId) {

		if(!authUser) {
			console.error("Please login first. Local storage seems empty.");
			return false;
		}

		socket.emit('request join', {
			gameId: gameId,
			player: authUser
		});
	}
});
