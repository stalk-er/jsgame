angular.module(
        'sampleApp',
        [
            'ngRoute',
            'appRoutes',
            'AppCtrl',
            'MainCtrl',
            'LoginCtrl',
            'NewGameCtrl',
            'FindGameCtrl',
            'OptionsCtrl',
            'ExitCtrl',
            'GameCtrl',
            'GameLobbyCtrl',
            'NerdService',
            'btford.socket-io'
        ])
        .factory('ENV', function () {

            // console.log(ENV) // env is loaded from the express server
            return {};
        })
        .factory('socket', function (socketFactory, ENV) {

            var myIoSocket = io.connect("http://localhost:8080");

            mySocket = socketFactory({
                ioSocket: myIoSocket
            });

            return mySocket;
        })
        .factory('authUser', function () {

            var user = JSON.parse(localStorage.getItem("user"));

            return user;
        })
        .factory('ENEMIES', function () {

            var ENEMIES = [

                {
                    name: "Mojiga",
                    type: "Primary",
                    hitpoints: 10,
                    healthpoints: 100,
                    //mtl: "gameObjects/3DModels/" + "/bad-ass/untitled-scene.mtl",
                    //obj: "gameObjects/3DModels/" + "/bad-ass/untitled-scene.obj",
                    //position: "0 -1.2 1"
                },
                {
                    name: "Headcrab",
                    type: "Giant",
                    hitpoints: 50,
                    healthpoints: 300,
                    //mtl: "gameObjects/3DModels/" + "/bad-ass/untitled-scene.mtl",
                    //obj: "gameObjects/3DModels/" + "/bad-ass/untitled-scene.obj",
                    //position: "0 -1.2 1"
                },
                {
                    name: "Blood flower",
                    type: "Champion",
                    hitpoints: 20,
                    healthpoints: 200,
                    //mtl: "gameObjects/3DModels/" + "/bad-ass/untitled-scene.mtl",
                    //obj: "gameObjects/3DModels/" + "/bad-ass/untitled-scene.obj",
                    //position: "0 -1.2 1"
                }
            ];

            return ENEMIES;
        })
        .factory('gameCharacters', function () {

            var characters = [

                /* Bad ass hero */
                {
                    name: "Bad Ass",
                    mtl: "gameObjects/3DModels/" + "/bad-ass/untitled-scene.mtl",
                    obj: "gameObjects/3DModels/" + "/bad-ass/untitled-scene.obj",
                    position: "0 -1.2 1"
                },
                /* Smart man */
                {
                    name: "Smart ass",
                    mtl: "gameObjects/3DModels/" + "/smart-ass/untitled-scene.mtl",
                    obj: "gameObjects/3DModels/" + "/smart-ass/untitled-scene.obj",
                    position: "0 -0.5 1.7"
                }
            ];

            return characters;
        });
