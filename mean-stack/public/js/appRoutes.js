// public/js/appRoutes.js
angular.module('appRoutes', [])
.config([
    '$routeProvider',
    '$locationProvider',
    function($routeProvider, $locationProvider, $stateProvider) {

        $routeProvider
        // Login view
        .when('/login', {
            templateUrl: 'views/login.html',
            controller: 'LoginController'
        })
        // new game view, where the user creates a new game
        .when('/new-game', {
            templateUrl: 'views/newGame.html',
            controller: 'NewGameController'
        })
        // find games view, where the user chooses from already created games
        .when('/find-games', {
            templateUrl: 'views/findGame.html',
            controller: 'FindGameController',
        })
        // options view, where the user changes his options for the game / current session
        // going to use Local Storage for the settings
        .when('/options', {
            templateUrl: 'views/options.html',
            controller: 'OptionsController'
        })
        // game lobby view is shown when you join a game and it's time to choose a hero and nickname and so on
        .when('/game/:gameId/lobby', {
            templateUrl: 'views/gameLobby.html',
            controller: 'GameLobbyController'
        })
        // game lobby view is shown when you join a game and it's time to choose a hero and nickname and so on
        .when('/game/:gameId', {
            templateUrl: 'views/game.html',
            controller: 'GameController'
        })
        .otherwise({
            redirectTo: '/'
        });

        $locationProvider.html5Mode(true);
    }])
    .run(function (authUser, $location, $rootScope) {

        $rootScope.$on('$routeChangeStart', function () {
            if(!authUser)
                $location.url('/login');
        });
    });
