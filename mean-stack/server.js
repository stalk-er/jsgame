// modules =================================================
var express        = require('express');
var app            = express();
var mongoose       = require('mongoose');
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');
var io = require('socket.io');
var env = require('./config/env')();

// configuration ===========================================

// config files
var db = require('./config/db');
var port = process.env.PORT || env.port; // set our port
mongoose.connect(db.url); // connect to our mongoDB database (commented out after you enter in your own credentials)

// get all data/stuff of the body (POST) parameters
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded
app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users

app.use("/configuration", express.static(__dirname + '/config/')); // serve the Env variable
app.use("/aframe-fps-look", express.static(__dirname + '/node_modules/aframe-no-click-look-controls/dist/')); // serve the aframe FPS component
app.get("/fps-component", function (req, res) {
        res.send(NoClickLookControls);
});

// routes ==================================================
require('./app/routes')(app); // pass our application into our routes

// Socket io ==================================================
io = io.listen(
    app.listen(port)
);

var games = [];
io.on('connection', function(socket) {

    // List available games
    socket.on('find games', function (emptyObj) {
        socket.emit('available games', games);
    });

    // Join to a game function
    function joinGame(socket, game, player) {

        // Push the host of the game as a first member in the game
        game.players.push(player);

        // Join the player to this game
        socket.join(game.id);
        console.log("GAME ID", game.id)

        // Broadcast the host of the game to his own game
        socket.broadcast.in(socket.id).emit('game players', {
            players: game.players,
        });

        // Respond to the user that he just joined the requested game
        socket.emit('join game', game);
    }


    // Request to leave a particular game
    // TODO implement force save in case the connection drops
    socket.on('leave game', function (leaveGameData) {

        var player = leaveGameData.player;
        var gameId = leaveGameData.gameId;
        var game = findGameById(gameId, games);

        if(game) {

            var player = findPlayerById(player.id, game.players);
            if(player) {

                // Leave the game
                socket.leave(game.id);

                console.log(game.players.length);

                // Remove from the game players
                for(var p in game.players) {
                    if(player.id == game.players[p].id) {
                        var index = game.players.indexOf(
                            game.players[p]
                        );
                        game.players.splice(index, 1);
                    }
                }

                console.log(game.players.length);

                // If no other players in this game. Delette the game
                if(game.players.length == 0) {
                    // Remove the game from the list of games
                    for(var g in games) {
                        if(game.id == games[g].id) {
                            var index = games.indexOf(
                                game
                            );
                            games.splice(index, 1);
                            break;
                        }
                    }
                }

                // TODO: save the user progress in the game
                // and exit
                socket.emit('exit game', {})
            }

            // Update the list of available games to join in (for other players)
            io.sockets.emit('available games', games);
        }
    });

    // Request to join in a game
    socket.on('request join', function (data) {

        var game = findGameById(data.gameId, games);
        if(game) {

            // If the game has a password, require authentication
            if(typeof game.password != "undefined" && game.password != "") {

                // Verify the user password, if correct join him into the game
                // this event won't execute if the game has no password
                socket.on('authenticate game', function (password) {

                    if(Buffer.from(password).toString('base64') == game.password) // passwords are matching
                    {
                        console.log("Password is correct");
                        joinGame(socket, game, data.player);
                    }
                    else socket.emit('authenticate', game);
                });
            } else {
                // If the game has no password, just join the player to the game
                joinGame(socket, game, data.player);
            }
        }
    });

    // Create new game
    socket.on('new game', function (data) {

        var gameData = data.gameData;
        var player = data.player;

        // Create a collection of all the players inside this game
        gameData.players = [];

        if(typeof gameData.password != "undefined" && gameData.password != "")
        {
            // Hash the password of the game if any
            gameData.password = Buffer.from(gameData.password).toString('base64');
        }

        // Set the id of the game as the socket id of the host / creator of the game
        gameData.id = player.id + socket.id;

        // Push the host of the game as a first member in the game
        gameData.players.push(player);

        // Join the host of the game to his own game
        socket.join(player.id);

        // Add the newly created game to the list of existing games
        games.push(gameData);

        // Broadcast the host of the game to his own game
        socket.broadcast.to(socket.id).emit('game players', {
            players: gameData.players,
        });

        // Update the list of available games to join in (for other players)
        io.sockets.emit('available games', games);

        // The game has been created, tell the client that it can be now redirected to the game view
        socket.emit('new game created', gameData);
    });

    function findPlayerById(id, players) {

        var player = null;
        for(var g in players) {
            if(players[g].id == id) {
                player = players[g];
                break;
            }
        }
        return player;
    }

    function findGameById(id, games) {
        var game = null;
        for(var g in games) {
            if(games[g].id == id) {
                game = games[g];
                break;
            }
        }
        return game;
    }

    // Somebody wants to find a game with that id. Give him.
    socket.on('find game by id', function (gameId) {
        console.log(findGameById(gameId, games))
        socket.emit("game exists", findGameById(gameId, games));
    });

    socket.on('disconnect', function() {

        console.log("Socket disconnected", socket);
        // for(var g in games) {
        //     for(var p in games[g].players) {
        //         if(games[g].players[p].id == socket.id) {
        //             socket.leave(games[g].id);
        //             var index = games[g].players.indexOf(
        //                 games[g].players[p]
        //             );
        //             games[g].players.splice(index, 1);
        //
        //             if(games[g].players.length == 0) {
        //                 var index = games.indexOf(
        //                     games[g]
        //                 );
        //                 games.splice(index, 1);
        //                 break;
        //             }
        //         }
        //     }
        // }

        // Update the list of available games to join in (for other players)
        io.sockets.emit('available games', games);

        //   io.sockets.emit('updateusers', usernames);
        //   socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' has disconnected');
    });
});
// End Socket io ==================================================



// start app ===============================================
//app.listen(port);
// server.listen(port, function () {
//
// });
console.log('Magic happens on port ' + port); 			// shoutout to the user
exports = module.exports = app; 						// expose app
